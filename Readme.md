## QR Code Generator

- Command Line Application to generate QR Codes

- Read Path = `Files`, Save Path = `Codes`

- Information on the `qrcode` library can be found on the [official pypi page](https://pypi.org/project/qrcode/)

<br>

### **CLI Arguments**

<br>

<pre>
1. --data | -d        : A Link or a text file placed in Read Path
2. --version | -v    
3. --box-size | -bxs 
4. --border | -b
5. --background | -bg : Background Color (Default: (255, 255, 255)) [Expects 'number' or 'number,number,number']
6. --foreground | -fg : Foreground Color (Default: (0, 0, 0)) [Expects 'number' or 'number,number,number']
7. --save | -s        : Saves the Code (Expects 'filename')
</pre>